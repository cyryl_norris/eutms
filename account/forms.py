from django import forms
from django.contrib.auth import get_user_model
from django.db.models import Q
from .models import UserProfile, Users

User = get_user_model()


class UserCreationForm(forms.ModelForm):
    StaffId = forms.IntegerField(label='StaffId', widget=forms.TextInput(attrs={'placeholder': 'Enter Staff ID'}))
    Email = forms.EmailField(label='Email', widget=forms.TextInput(attrs={'placeholder': 'Enter Email'}))
    Name = forms.CharField(label='Name', widget=forms.TextInput(attrs={'placeholder': 'Enter  Name'}))
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'placeholder': 'Enter password'}))
    password2 = forms.CharField(label='Password Confirmation',
                                widget=forms.PasswordInput(attrs={'placeholder': 'Confirm password'}))

    class Meta:
        model = UserProfile
        fields = ['StaffId', 'Email', 'Name']

    def clean_password(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Password so not match')
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])

        if commit:
            user.save()
        return user


class UserLoginForm(forms.Form):
    StaffId = forms.IntegerField(label='StaffId', widget=forms.TextInput(attrs={'placeholder': 'Enter Staff ID'}))
    password = forms.CharField(label='password', widget=forms.PasswordInput(attrs={'placeholder': 'Enter password'}))

    def clean(self, *args, **kwargs):
        StaffId = self.cleaned_data.get('StaffId')
        password = self.cleaned_data.get('password')
        user_qs_final = User.objects.filter(
            StaffId=StaffId
        )
        if not user_qs_final.exists() and user_qs_final.count != 1:
            raise forms.ValidationError("invalid credentials-user doesnt exist")
        user_obj = user_qs_final.first()
        if not user_obj.check_password(password):
            raise forms.ValidationError('credentials are not correct')
        self.cleaned_data["user_obj"] = user_obj
        return super(UserLoginForm, self).clean(*args, **kwargs)


class UserProfileForm(forms.ModelForm):
    DEPT_TYPES = (
        ("ComputerScience", "Computer Science"),
        ("Physics", "Physics"),
        ("Geography", "Geography"),
        ("Mathematics", "Mathematics"),
        ("Halls", "Halls"),
        ("Gender", "Gender"),
        ("Transport", "Transport"),
        ("Procurement", "Procurement"),
    )
    Department = forms.CharField(widget=forms.Select(
        choices=DEPT_TYPES,
    ), )
    Contact = forms.CharField()

    class Meta:
        model = UserProfile
        fields = ('StaffId', 'Role', 'Name', 'Email', 'Contact', 'Department',)

    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)
        self.fields['StaffId'].disabled = True
        self.fields['Role'].disabled = True
        self.fields['Name'].disabled = True
        # self.fields['Department'].disabled = True


class StaffForm(forms.ModelForm):
    DEPT_TYPES = (
        ("Transport", "Transport"),
    )
    Department = forms.CharField(widget=forms.Select(
        choices=DEPT_TYPES,
    ), )
    StaffId = forms.IntegerField(label='StaffId', widget=forms.TextInput(attrs={'placeholder': 'Enter Staff ID'}))
    Name = forms.CharField(label='Name', widget=forms.TextInput(attrs={'placeholder': 'Enter  Name'}))
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'placeholder': 'Enter password'}))
    password2 = forms.CharField(label='Password Confirmation',
                                widget=forms.PasswordInput(attrs={'placeholder': 'Confirm password'}))

    class Meta:
        model = UserProfile
        fields = ['StaffId', 'Role', 'Name','Email','Contact','Department']

    def clean_password(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Password so not match')
        return password2

    def save(self, commit=True):
        user = super(StaffForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password2'])

        if commit:
            user.save()
        return user
