from django.db import models
from django.core.validators import RegexValidator
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)

STAFFID_REGEX = '^[a-zA-Z0-9.+-]*$'
CONTACT_REGEX = '^\d+$'


class MyUserManager(BaseUserManager):
    def create_user(self, StaffId, password=None):
        user = self.model(
            StaffId=StaffId,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user
        # user.password = password //bad-dont do this

    def create_superuser(self, StaffId, password=None):
        user = self.create_user(
            StaffId, password=password
        )
        user.is_staff = True
        user.save(using=self._db)
        return user


class Users(AbstractBaseUser):
    StaffId = models.IntegerField(
        primary_key=True,
        unique=True)

    is_staff = models.BooleanField(default=False)

    objects = MyUserManager()

    USERNAME_FIELD = 'StaffId'

    def __str__(self):
        return self.StaffId

    def get_short_name(self):
        # user is identified by their email address
        return self.StaffId

    def has_perm(self, perm, odj=None):
        "Does the user have a specific permissions"
        # Simplest possible answer :Yes,always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app 'app_label'?"
        # Simplest possible answer :Yes,always
        return True


class UserProfile(Users):
    ADMIN = 1,
    TransportManager = 2
    FleetAssistant = 3
    DepartmentHead = 4
    MechanicManager = 5
    ROLE_CHOICES = (
        (1, 'Admin'),
        (2, 'Transport Manager'),
        (3, 'Fleet Assistant'),
        (4, 'Department Head'),
        (5, 'Mechanic Manager')
    )
    Email = models.EmailField(
        max_length=100,
        primary_key=True,
        verbose_name='email address'
    )
    Role = models.PositiveIntegerField(choices=ROLE_CHOICES, null=True, blank=True, default=4)
    Contact = models.CharField(
        max_length=13,
        validators=[
            RegexValidator(regex=CONTACT_REGEX,
                           message="invalid mobile number",
                           code='invalid_contact')
        ],
    )
    Name = models.CharField(max_length=200)
    Department = models.CharField(max_length=100)

    def __str__(self):
        return self.Department
# def create_profile(sender, **kwargs):
# if kwargs['created']:
#     user_profile = Staff.objects.create(user = kwargs['instance'])
#
# post_save.connect(create_profile, sender=Users)

# @receiver(post_save, sender=Users)
# def create_user_profile(sender, instance, created, **kwargs):
#   if created:
#      Staff.objects.create(user=instance)
#  instance.profile.save()


# @receiver(post_save, sender=Users)
# def create_student_profile(sender, instance, created, **kwargs):
#     if created:
#         UserProfile.objects.create(user=instance)
#
#
# @receiver(post_save, sender=Users)
# def save_student_profile(sender, instance, **kwargs):
#     instance.userprofile.save()
