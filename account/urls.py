from django.conf.urls import url
from django.contrib.auth.views import password_reset, password_reset_done
from . import views

urlpatterns = [
    url(r'^$', views.login_view, name='login'),
    # url(r'^reset-password/$', password_reset, name='reset_password'),
    # url(r'^reset-password/done/$', password_reset_done, name='password_reset_done'),
    url(r'^password', views.change_password, name='change_password'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^signup/$', views.register, name='signup'),
    url(r'^profile/$', views.profile, name='Profile'),
    # url(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
    # url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    # url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
    #     auth_views.password_reset_confirm, name='password_reset_confirm'),
    # url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),

]
