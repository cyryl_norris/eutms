from django.shortcuts import render, redirect
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.db import transaction
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import login, logout
from .forms import UserLoginForm, UserCreationForm, UserProfileForm,StaffForm


# Create your views here.

def register(request):
    form = UserCreationForm(request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/")
    context = {
        'form': form
    }
    return render(request, 'account/register.html', context)





def pass_reset(request):
    return render(request, 'account/forgotpassword.html')


def login_view(request):
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        user_obj = form.cleaned_data.get('user_obj')
        login(request, user_obj)
        if request.user.userprofile.Role is 1:
            return redirect('admin')
        elif request.user.userprofile.Role is 2:
            return redirect('admin')
        elif request.user.userprofile.Role is 3:
            return redirect('fleetassistant')
        elif request.user.userprofile.Role is 4:
            return redirect('user')
        elif request.user.userprofile.Role is 5:
            return redirect('mechanic')
    return render(request, "account/login.html", {"form": form})


@login_required
def profile(request):
    if request.method == 'POST':
        profile_form = UserProfileForm(request.POST, instance=request.user.userprofile)
        if profile_form.is_valid():
            profile_form.save()
            messages.success(request, ('Your profile was successfully updated!'))
            if request.user.userprofile.Role is 1:
                return redirect('admin')
            elif request.user.userprofile.Role is 2:
                return redirect('admin')
            elif request.user.userprofile.Role is 3:
                return redirect('fleetassistant')
            elif request.user.userprofile.Role is 4:
                return redirect('user')
            elif request.user.userprofile.Role is 5:
                return redirect('mechanic')
        else:
            messages.error(request, ('Please correct the error below.'))
    else:
        profile_form = UserProfileForm(instance=request.user.userprofile)
    if request.user.userprofile.Role is 2:
        return render(request, 'account/tmprofile.html', {
            'profile_form': profile_form
        })
    elif request.user.userprofile.Role is 3:
        return render(request, 'account/fleetprofile.html', {
            'profile_form': profile_form
        })
    elif request.user.userprofile.Role is 4:
        return render(request, 'account/userprofile.html', {
            'profile_form': profile_form
        })
    elif request.user.userprofile.Role is 5:
        return render(request, 'account/mechanicprofile.html', {
            'profile_form': profile_form
        })


def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            if request.user.userprofile.Role is 1:
                return redirect('admin')
            elif request.user.userprofile.Role is 2:
                return redirect('admin')
            elif request.user.userprofile.Role is 3:
                return redirect('fleetassistant')
            elif request.user.userprofile.Role is 4:
                return redirect('user')
            elif request.user.userprofile.Role is 5:
                return redirect('mechanic')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    if request.user.userprofile.Role is 2:
        return render(request, 'account/tm_change_pass.html', {
            'form': form
        })
    elif request.user.userprofile.Role is 3:
        return render(request, 'account/fleet_change_pass.html', {
            'form': form
        })
    elif request.user.userprofile.Role is 4:
        return render(request, 'account/user_change_pass.html', {
            'form': form
        })
    elif request.user.userprofile.Role is 5:
        return render(request, 'account/mechanic_change_pass.html', {
            'form': form
        })


def logout_view(request):
    logout(request)
    return HttpResponseRedirect("/")
