from django import forms
from .models import Vehicle, Request, SparePart, Driver, Mechanic, MechanicAllocation, BusAllocation, \
    SparePartsAllocation


class VehiclesForm(forms.ModelForm):
    VEHICLE_TYPE = (
        ("Bus", "Bus"),
        ("Double Cabin", "Double Cabin"),
    )
    Number_plate = forms.CharField(min_length=7, max_length=7)
    Vehicle_type = forms.CharField(widget=forms.Select(
        choices=VEHICLE_TYPE,
    ),)
    Engine_capacity = forms.CharField()
    Capacity = forms.IntegerField()

    class Meta:
        model = Vehicle
        fields = ('Number_plate', 'Vehicle_type', 'Engine_capacity', 'Capacity')


class RequestForm(forms.ModelForm):
    DEPT_TYPES = (
        ("ComputerScience", "Computer Science"),
        ("Physics", "Physics"),
        ("Geography", "Geography"),
        ("Mathematics", "Mathematics"),
        ("Halls", "Halls"),
        ("Gender", "Gender"),
        ("Transport", "Transport"),
        ("Procurement", "Procurement"),
    )
    TRAVELLER_TYPES = (
        ("Students", "Students"),
        ("Staff", "Staff"),
    )
    DeptRequesting = forms.CharField(widget=forms.Select(
        choices=DEPT_TYPES,
    ), )
    Reason = forms.CharField()
    Travel_date = forms.DateField(widget=forms.TextInput(attrs={"class": "datepicker"}))
    Return_date = forms.DateField(widget=forms.TextInput(attrs={"class": "datepicker"}))
    Destination = forms.CharField()
    Travellers_desc = forms.CharField(widget=forms.Select(
        choices=TRAVELLER_TYPES,
    ), )
    Capacity = forms.IntegerField()

    class Meta:
        model = Request
        widgets = {
            'User': forms.HiddenInput,
            'DeptRequesting': forms.HiddenInput,
        }
        exclude = ['User']
        fields = (
            'Travel_date', 'Return_date', 'Destination', 'Travellers_desc', 'Reason', 'Capacity')


class SparePartForm(forms.ModelForm):
    Name = forms.CharField()
    Amount = forms.IntegerField()
    Cost = forms.IntegerField()
    Description = forms.TextInput()

    class Meta:
        model = SparePart
        fields = ('Name', 'Amount', 'Cost', 'Description')


class DriverForm(forms.ModelForm):
    DEPT_TYPES = (
        ("Transport", "Transport"),
    )
    Department = forms.CharField(widget=forms.Select(
        choices=DEPT_TYPES,
    ), )
    StaffId = forms.IntegerField()
    Email = forms.EmailField()
    Contact = forms.CharField(max_length=10, min_length=10)
    Name = forms.CharField()

    class Meta:
        model = Driver
        fields = ('StaffId', 'Email', 'Contact', 'Name', 'Department')


class MechanicForm(forms.ModelForm):
    DEPT_TYPES = (
        ("Transport", "Transport"),
    )
    Department = forms.CharField(widget=forms.Select(
        choices=DEPT_TYPES,
    ), )
    StaffId = forms.IntegerField()
    Email = forms.EmailField()
    Contact = forms.CharField(max_length=10, min_length=10)
    Name = forms.CharField()

    class Meta:
        model = Mechanic
        fields = ('StaffId', 'Email', 'Contact', 'Name', 'Department')


class MechanicAllocationForm(forms.ModelForm):
    date = forms.DateField(widget=forms.TextInput(attrs={"class": "datepicker"}))

    class Meta:
        model = MechanicAllocation
        fields = ('mechanic', 'vehicle', 'date')


class SparepartAllocationForm(forms.ModelForm):
    date = forms.DateField(widget=forms.TextInput(attrs={"class": "datepicker"}))

    class Meta:
        model = SparePartsAllocation
        fields = ('sparepart', 'vehicle', 'date')


class BusAllocationform(forms.ModelForm):
    STATUS = (
        ('Pending', 'Pending'),
        ('Confirmed', 'Confirmed'),
        ('Rejected', 'Rejected'),
    )
    Confirm_status = forms.CharField(widget=forms.Select(
        choices=STATUS,
    ), )

    class Meta:
        model = BusAllocation

        # widgets = {
        #     'User': forms.TextInput(attrs={'disabled': True}),
        #     'Reason': forms.TextInput(attrs={'disabled': True}),
        #     'Capacity': forms.TextInput(attrs={'disabled': True}),
        #     'Travel_date': forms.TextInput(attrs={'disabled': True}),
        #     'Travellers_desc': forms.TextInput(attrs={'disabled': True}),
        #     'Destination': forms.TextInput(attrs={'disabled': True}),
        #
        # }

        fields = ('Request', 'Vehicle',
                  'Driver', 'Driver_fee', 'Estimated_distance',
                  'Fuel_money', 'Confirm_status')
