from django.contrib.admin.templatetags.admin_list import pagination
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from .forms import VehiclesForm, RequestForm, SparePartForm, DriverForm, \
    MechanicForm, MechanicAllocationForm, SparepartAllocationForm, BusAllocationform
from account.forms import StaffForm, UserProfileForm, UserCreationForm
from .models import Vehicle, Request, SparePart, Driver, Mechanic, BusAllocation
from account.models import Users, UserProfile


# TRANS_MANAGER######################################################################################
@login_required(login_url='/')
def admin(request):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    vehicleCount = Vehicle.objects.all().count()
    driverCount = Driver.objects.all().count()
    userCount = Users.objects.all().count()
    data = Vehicle.objects.all()
    data1 = Driver.objects.all()
    data2 = UserProfile.objects.all()
    data3 = BusAllocation.objects.all()
    if request.POST:
        form = VehiclesForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("admin")
        print(vehicleCount)
    else:
        form = VehiclesForm()
    return render(request, 'account/admin.html',
                  {"form": form, "data": data, "data1": data1, "data2": data2, "data3": data3,
                   "vehicleCount": vehicleCount,
                   "driverCount": driverCount, "userCount": userCount, 'user': user})


def admin_search(request):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    vehicleCount = Vehicle.objects.all().count()
    driverCount = Driver.objects.all().count()
    userCount = Users.objects.all().count()
    data = Vehicle.objects.all()
    data1 = Driver.objects.all()
    data2 = UserProfile.objects.all()
    data3 = BusAllocation.objects.all()
    query = request.GET.get('qq')

    user_results = UserProfile.objects.filter(
        Q(Name__contains=query) | Q(Department__contains=query))
    driver_results = Driver.objects.filter(Q(Name__contains=query))

    return render(request, 'account/admin.html',
                  {'user_results': user_results, "driver_results": driver_results, "data": data, "data1": data1,
                   "data2": data2, "data3": data3,
                   "vehicleCount": vehicleCount,
                   "driverCount": driverCount, "userCount": userCount, 'user': user})


def admin_schedule(request):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    vehicleCount = Vehicle.objects.all().count()
    driverCount = Driver.objects.all().count()
    userCount = Users.objects.all().count()
    data = Vehicle.objects.all()
    data1 = Driver.objects.all()
    data2 = UserProfile.objects.all()
    data3 = BusAllocation.objects.all()
    return render(request, 'account/schedule.html',
                  {"data": data, "data1": data1, "data2": data2, "data3": data3,
                   "vehicleCount": vehicleCount,
                   "driverCount": driverCount, "userCount": userCount, 'user': user})


@login_required(login_url='/')
def delete_user(request, pk):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    vehicleCount = Vehicle.objects.all().count()
    driverCount = Driver.objects.all().count()
    userCount = Users.objects.all().count()
    data = Vehicle.objects.all()
    data1 = Driver.objects.all()
    data2 = UserProfile.objects.all()
    data3 = BusAllocation.objects.all()
    dlt = get_object_or_404(UserProfile, pk=pk)
    form = UserProfileForm(request.POST or None, instance=dlt)

    if form.is_valid():
        dlt.delete()
        return redirect("admin")
    else:
        form = UserProfileForm(instance=dlt)

    return render(request, 'account/user_dlt.html',
                  {'form': form, "data": data, "data1": data1, "data2": data2, "data3": data3,
                   "vehicleCount": vehicleCount, "driverCount": driverCount, "userCount": userCount, 'user': user})


def driver_delete(request, pk):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    vehicleCount = Vehicle.objects.all().count()
    driverCount = Driver.objects.all().count()
    userCount = Users.objects.all().count()
    data = Vehicle.objects.all()
    data1 = Driver.objects.all()
    data2 = UserProfile.objects.all()
    data3 = BusAllocation.objects.all()
    driver = get_object_or_404(Driver, pk=pk)
    form = DriverForm(request.POST or None, instance=driver)

    if form.is_valid():
        driver.delete()
        return redirect("admin")
    else:
        form = DriverForm(instance=driver)
        return render(request, 'account/driver_dlt.html',
                      {"form": form, "data": data, "data1": data1, "data2": data2, "data3": data3,
                       "vehicleCount": vehicleCount, "driverCount": driverCount, "userCount": userCount, 'user': user})


def vehicle_delete(request, pk):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    vehicleCount = Vehicle.objects.all().count()
    driverCount = Driver.objects.all().count()
    userCount = Users.objects.all().count()
    data = Vehicle.objects.all()
    data1 = Driver.objects.all()
    data2 = UserProfile.objects.all()
    data3 = BusAllocation.objects.all()
    vehicle = get_object_or_404(Vehicle, pk=pk)
    form = VehiclesForm(request.POST or None, instance=vehicle)

    if form.is_valid():
        vehicle.delete()
        return redirect("admin")
    else:
        form = VehiclesForm(instance=vehicle)
        return render(request, 'account/vehicle_dlt.html',
                      {"form": form, "data": data, "data1": data1, "data2": data2, "data3": data3,
                       "vehicleCount": vehicleCount, "driverCount": driverCount, "userCount": userCount, 'user': user})


@login_required(login_url='/')
def add_driver(request):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    vehicleCount = Vehicle.objects.all().count()
    driverCount = Driver.objects.all().count()
    userCount = Users.objects.all().count()
    data = Vehicle.objects.all()
    data1 = Driver.objects.all()
    data2 = UserProfile.objects.all()
    data3 = BusAllocation.objects.all()
    if request.POST:
        form = DriverForm(request.POST)
        if form.is_valid():
            form.save()
            print("Added Driver")
            return redirect("admin")
    else:
        print("Did not add driver")
        form = DriverForm(request.POST)
        return render(request, 'account/add_driver.html',
                      {"form": form, "data": data, "data1": data1, "data2": data2, "data3": data3,
                       "vehicleCount": vehicleCount, "driverCount": driverCount, "userCount": userCount, 'user': user})


def staff_creation(request):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    vehicleCount = Vehicle.objects.all().count()
    driverCount = Driver.objects.all().count()
    userCount = Users.objects.all().count()
    data = Vehicle.objects.all()
    data1 = Driver.objects.all()
    data2 = UserProfile.objects.all()
    data3 = BusAllocation.objects.all()
    form = StaffForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("admin")
    context = {
        'form': form, "data": data, "data1": data1, "data2": data2, "data3": data3,
        "vehicleCount": vehicleCount, "driverCount": driverCount, "userCount": userCount, 'user': user
    }
    return render(request, 'account/staff_creation.html', context)


# FLEET_HEAD######################################################################################

@login_required(login_url='/')
def fleetassistant(request):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    vehicleCount = Vehicle.objects.all().count()
    driverCount = Driver.objects.all().count()
    requestCount = Request.objects.all().count()
    data4 = BusAllocation.objects.filter(Confirm_status__contains="Confirmed")
    data = Request.objects.all()
    data1 = Vehicle.objects.all()
    data2 = Driver.objects.all()
    return render(request, 'account/fleetassistant.html',
                  {'data': data, 'data1': data1, 'data2': data2, "data4": data4, "requestCount": requestCount,
                   "driverCount": driverCount, "vehicleCount": vehicleCount, 'user': user})


@login_required(login_url='/')
def fleet_view(request):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    vehicleCount = Vehicle.objects.all().count()
    driverCount = Driver.objects.all().count()
    requestCount = Request.objects.all().count()
    data = Request.objects.all()
    data1 = Vehicle.objects.all()
    data2 = Driver.objects.all()
    data3 = BusAllocation.objects.all()
    data4 = BusAllocation.objects.filter(Confirm_status="Confimred")
    if request.POST:
        form = BusAllocationform(request.POST)
        if form.is_valid():
            form.save()
        return redirect('fleetassistant')
    else:
        form = BusAllocationform()
    return render(request, 'account/fleetview.html',
                  {'form': form, 'data': data, 'data1': data1, 'data2': data2, 'data3': data3, "data4": data4,
                   "requestCount": requestCount,
                   "driverCount": driverCount, "vehicleCount": vehicleCount, 'user': user})


def fleet_search(request):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    vehicleCount = Vehicle.objects.all().count()
    driverCount = Driver.objects.all().count()
    requestCount = Request.objects.all().count()
    data = Request.objects.all()
    data1 = Vehicle.objects.all()
    data2 = Driver.objects.all()
    data3 = BusAllocation.objects.all()
    data4 = BusAllocation.objects.filter(Confirm_status="Confimred")
    query = request.GET.get('q')

    results = Request.objects.filter(Q(DeptRequesting__icontains=query) | Q(User__Name__icontains=query))
    if request.POST:
        form = BusAllocationform(request.POST)
        if form.is_valid():
            form.save()
        return redirect('fleetassistant')
    else:
        form = BusAllocationform()
    return render(request, 'account/fleet_search.html',
                  {'results': results, "form": form, 'data': data, 'data1': data1, 'data2': data2, 'data3': data3,
                   "data4": data4, "requestCount": requestCount,
                   "driverCount": driverCount, "vehicleCount": vehicleCount, 'user': user})


def bus_allocation(request):
    if request.POST:
        form = BusAllocationform(request.POST)
        if form.is_valid():
            form.save()
        return redirect('fleetassistant')
    else:
        form = BusAllocationform()
    return render(request, 'account/fleetview.html', {"form": form})


# DEPT_HEAD######################################################################################
@login_required(login_url='/')
def user(request):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    req_count = Request.objects.filter(User=request.user.userprofile).count()
    if request.POST:
        form = RequestForm(request.POST)
        if form.is_valid():
            req = form.save(commit=False)
            req.User = UserProfile.objects.get(Name=request.POST.get('User', request.user.userprofile.Name))
            print(request.user.userprofile.StaffId)
            req.save()
            return redirect('user')
    else:
        form = RequestForm()
    return render(request, 'account/user.html', {"form": form, 'user': user, 'req_count': req_count})


def req_changes(request):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    req_count = Request.objects.filter(User=request.user.userprofile).count()
    reqs = Request.objects.filter(User=request.user.userprofile)
    return render(request, 'account/requpdate.html', {'reqs': reqs, 'user': user, 'req_count': req_count})


def req_update(request, pk):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    req_count = Request.objects.filter(User=request.user.userprofile).count()
    change = get_object_or_404(Request, pk=pk)
    form = RequestForm(request.POST or None, instance=change)

    if form.is_valid():
        form.save()
        return redirect("user")
    else:
        form = RequestForm(instance=change)
        return render(request, 'account/update.html', {"form": form, 'user': user, 'req_count': req_count})


def req_delete(request, pk):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    req_count = Request.objects.filter(User=request.user.userprofile).count()
    change = get_object_or_404(Request, pk=pk)
    form = RequestForm(request.POST or None, instance=change)

    if form.is_valid():
        change.delete()
        return redirect("user")
    else:
        form = RequestForm(instance=change)
        return render(request, 'account/req_delete.html', {"form": form, 'user': user, 'req_count': req_count})


def booking_status(request):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    req_count = Request.objects.filter(User=request.user.userprofile).count()
    user = request.user.userprofile
    booking = Request.objects.filter(User=user)

    return render(request, 'account/booking_status.html', {'booking': booking, 'user': user, 'req_count': req_count})


# MEC_MANAGER######################################################################################
@login_required(login_url='/')
def mechanic(request):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    spCount = SparePart.objects.all().count()
    mechCount = Mechanic.objects.all().count()
    data = SparePart.objects.all()
    data1 = Mechanic.objects.all()
    if request.POST:
        form = SparePartForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect("mechanic")
    else:
        form = SparePartForm()
    return render(request, 'account/mechanic.html',
                  {"form": form, "data": data, "data1": data1, "spCount": spCount, "mechCount": mechCount,
                   'user': user})


@login_required(login_url='/')
def mechanics(request):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    spCount = SparePart.objects.all().count()
    mechCount = Mechanic.objects.all().count()
    data = SparePart.objects.all()
    data1 = Mechanic.objects.all()
    if request.POST:
        form = MechanicForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("mechanic")
    else:
        form = MechanicForm()
    return render(request, 'account/add_mechanic.html',
                  {"form": form, 'user': user, "data": data, "data1": data1, "spCount": spCount,
                   "mechCount": mechCount})


@login_required(login_url='/')
def mech_allocation(request):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    spCount = SparePart.objects.all().count()
    mechCount = Mechanic.objects.all().count()
    data = SparePart.objects.all()
    data1 = Mechanic.objects.all()
    if request.POST:
        form = MechanicAllocationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("mechanic")
    else:
        form = MechanicAllocationForm()
    return render(request, 'account/mech_allocation.html',
                  {'form': form, "user": user, "data": data, "data1": data1, "spCount": spCount,
                   "mechCount": mechCount})


@login_required(login_url='/')
def sp_allocation(request):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    spCount = SparePart.objects.all().count()
    mechCount = Mechanic.objects.all().count()
    data = SparePart.objects.all()
    data1 = Mechanic.objects.all()
    if request.POST:
        form = SparepartAllocationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("mechanic")
    else:
        form = SparepartAllocationForm()
    return render(request, 'account/sp_allocation.html',
                  {'form': form, "user": user, "data": data, "data1": data1, "spCount": spCount,
                   "mechCount": mechCount})


def mechanic_delete(request, pk):
    user = UserProfile.objects.get(Name=request.user.userprofile.Name)
    spCount = SparePart.objects.all().count()
    mechCount = Mechanic.objects.all().count()
    data = SparePart.objects.all()
    data1 = Mechanic.objects.all()
    mech = get_object_or_404(Mechanic, pk=pk)
    form = MechanicForm(request.POST or None, instance=mech)

    if form.is_valid():
        mech.delete()
        return redirect("mechanic")
    else:
        form = MechanicForm(instance=mech)
        return render(request, 'account/mechanic_dlt.html',
                      {"form": form, "user": user, "data": data, "data1": data1, "spCount": spCount,
                       "mechCount": mechCount})


def requests(request):
    return render(request, 'account/requests.html')


def add_vehicle(request):
    return render(request, 'account/user.html')


def remove_vehicle(request):
    return render(request, 'account/remove_vehicle.html')
